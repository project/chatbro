CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The ChatBro module allows the user to add a chat to our website, this chat can
also be linked to a telegram group.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/chatbro

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/chatbro


REQUIREMENTS
------------

This module no requires additional modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Create a chat in https://www.chatbro.com.
 * Add ChatBro ID here: mysite.com/chatbro/settings (This works with
   permissions, permission to modify chat settings).
 * Where is ChatBro ID? https://www.drupal.org/files/chatbro_0.PNG


MAINTAINERS
-----------

Current maintainers:
 * Antonio Sanchez (saesa) - https://www.drupal.org/user/3561094
